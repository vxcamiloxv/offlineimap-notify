(use-modules (gnu)
	     (guix profiles)
	     (gnu packages mail))

(concatenate-manifests
 (list (package->development-manifest offlineimap3)
       (specifications->manifest '("python"
                                   "python-pip"
                                   "python-wheel"))))
