# offlineimap-notify

The default branch is now
[main][main_branch]. check it to see the
most recent changes

Wrapper for add notification sending to OfflineIMAP.

Run OfflineIMAP after adding notification sending to its UIs.  When an account
finishes syncing, messages synced to the local repository will be reported
using D-Bus (through `notify-py`) or fallback `notifier` command from
configuration file.

## Quick Start

* Install the package
```sh
  pip3 install offlineimap-notify
```

* Call `offlineimap-notify` instead of `offlineimap`

```sh
 offlineimap-notify
```

## Requirements
* Python 3.10.x
* OfflineIMAP
* [notify-py][notifypy] (Optional)

## Usage
For configuration options and usage check [Docs](https://framagit.org/distopico/offlineimap-notify/-/blob/master/docs/offlineimap-notify.md)

## License
[![GNU General Public License version 3](https://www.gnu.org/graphics/gplv3-127x51.png)](https://framagit.org/distopico/offlineimap-notify/-/blob/master/LICENSE)

[main_branch]: https://framagit.org/distopico/offlineimap-notify
[notifypy]: https://pypi.org/project/notify-py
